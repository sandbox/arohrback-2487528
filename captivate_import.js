jQuery(document).ready(function () {
	jQuery('#wrapperFrame').load( function() {
        var wrapperDoc = jQuery('#wrapperFrame')[0].contentDocument;
		console.log(wrapperDoc);
		var ifr=jQuery('<iframe/>', {
            id:'CPModule',
            src:Drupal.settings.captivate_import.cpPath,
            style:'display: none; width: 100%; height: 100%; border: 0; margin: 0; padding: 0',
            load:function(){
                jQuery(this).show();
            }
        });
        jQuery(wrapperDoc).find('body').append(ifr);    
	});
});