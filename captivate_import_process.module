<?php

/**
 * @file
 * A module that converts Adobe Captivate packages into sets of content nodes.
 */

	/**
	* Implements hook_help().
	*
	* Displays help and module information.
	*
	* @param path
	*	which path of the site we're using to display help
	* @param arg
	*	Array that holds the current path as returned from arg() function
	*/

function captivate_import_process_help($path, $arg) {
	switch ($path) {
		case "admin/help#captivate_import":
			return '<p>' . t("Enables content types and taxonomies for Adobe Captivate import") . '</p>';
			break;
	}
}

/**
* Implements hook_menu().
*/
	 
function captivate_import_process_menu() {
	$items = array();
	
	$items['admin/config/media/captivate_import'] = array(
		'title' => 'Captivate Import',
		'description' => 'Configuration for Adobe Captivate project display',
		'page callback' => 'drupal_get_form',
		'page arguments' => array('captivate_import_process_config_form'),
		'access arguments' => array('upload captivate files'),
		'type' => MENU_NORMAL_ITEM,
	);
	$items['node/add/upload_lunit'] = array(
		'title' => 'Import from Captivate',
		'description' => 'Upload and convert an Adobe Captivate-published learning unit',
		'page callback' => 'drupal_get_form',
		'page arguments' => array('captivate_import_process_upload_form'),
		'access arguments' => array('upload captivate files'),
		'type' => MENU_LOCAL_TASK,
		'access callback' => TRUE,
	);
	
	return $items;
}

/**
 * Implements hook_permission().
 */
function captivate_import_process_permission() {
	return array(
		'upload captivate files' => array(
			'title' => t('Migrate Captivate files'),
			'description' => t('Upload and process Adobe Captivate-created ZIP files for conversion.'),
		),
	);
}

/**
 * Page callback: captivate_import settings
 *
 * @see captivate_import_process_menu()
 */
function captivate_import_process_config_form($form, &$form_state) {
	$form['captivate_import_folder'] = array(
		'#type' => 'textfield',
		'#title' => t('Root assets folder'),
		'#default_value' => variable_get('captivate_import_folder', ''),
		'#size' => 60,
		'#description' => t('Folder where Captivate assets are stored.'),
		'#required' => TRUE,
	);
	$form['captivate_import_image_folder'] = array(
		'#type' => 'textfield',
		'#title' => t('Migrated image folder'),
		'#default_value' => variable_get('captivate_import_image_folder', ''),
		'#size' => 60,
		'#description' => t('Folder to store images for use in migrated learning units.'),
		'#required' => TRUE,
	);
	$form['captivate_import_stopwords'] = array(
		'#type' => 'textarea',
		'#title' => t('Stopwords'),
		'#default_value' => variable_get('captivate_import_stopwords', ''),
		'#description' => t('Words that should be ignored for keyword indexing.'),
		'#required' => TRUE,
	);
	$form['captivate_import_repeat_threshhold'] = array(
		'#type' => 'textfield',
		'#title' => t('Image repeat threshhold'),
		'#default_value' => variable_get('captivate_import_repeat_threshhold', ''),
		'#description' => t('Percent of slides that an image must show up on to trigger the automatic removal filter.'),
		'#required' => TRUE,
	);
	return system_settings_form($form);
}

/**
 * Implements hook_form_FORM_ID_alter().
 **/
 
function captivate_import_process_form_lunit_node_form_alter(&$form, &$form_state, $form_id) {
	$form['lunit_asset_path']['#access'] = 0;
}


/**
 * Implements validation from the Form API.
 *
 * @param $form
 *	 A structured array containing the elements and properties of the form.
 * @param $form_state
 *	 An array that stores information about the form's current state
 *	 during processing.
 */
 
function captivate_import_process_config_form_validate($form, &$form_state){
	$folder_path = $form_state['values']['captivate_import_folder'];
	if (!is_dir($folder_path)) {
		form_set_error('captivate_import_folder', $folder_path.' '.t('This path does not exist.'));
	}
	$folder_path = $form_state['values']['captivate_import_image_folder'];
	if (!is_dir($folder_path)) {
		form_set_error('captivate_import_image_folder', $folder_path.' '.t('This path does not exist.'));
	}
}

/**
 * Implements drupal_get_form()
 */

function captivate_import_process_upload_form($form, &$form_state) {
	if ($form_state['rebuild']) {
		$form_state['input'] = array();
	}
	if (empty($form_state['storage'])) {
		// first step of upload form series
		$form_state['storage'] = array(
			'step' => 'captivate_import_process_upload_form_start',
		);
	}
	$function = $form_state['storage']['step'];
	$form = $function($form, $form_state);
	return $form;
}

function captivate_import_process_upload_form_submit($form, &$form_state) {
	dsm($form_state);
	$values = $form_state['values'];
	if (isset($values['back']) && $values['op'] == $values['back']) {
		// what to do if they went back.
	}
	else {
		$step = $form_state['storage']['step'];
		$form_state['storage']['steps'][] = $step;
		if (function_exists($step.'_submit')) {
			$function = $step. '_submit';
			$function($form, $form_state);
		}
	}
	return;
}

function captivate_import_process_upload_form_start($form, &$form_state) {	
	$query = db_select('taxonomy_vocabulary', 'tv');
	$query->fields('tv',array('vid'));
	$query->condition('tv.machine_name', 'courses', '=');
	$result = $query->execute();
	$resultRow = $result->fetchAssoc();
	$course_terms = taxonomy_get_tree($resultRow['vid']);
	$dropdown_options = array();
	foreach ($course_terms as $course) {
		$dropdown_options[$course->tid] = $course->name;
	}
	$form['zip_file'] = array(
		'#type' => 'managed_file',
		'#title' => t('ZIP file'),
		'#description' => t('Adobe Captivate-published ZIP file of HTML5 content'),
		'#upload_validators' => array(
			'file_validate_extensions' => array('zip'),
		)
	);
	$form['course'] = array(
		'#type' => 'select',
		'#title' => 'Course',
		'#description' => t('Course this learning unit belongs to.'),
		'#options' => $dropdown_options
	);
	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Next')
	);
	return $form;
}

function captivate_import_process_upload_form_start_submit($form, &$form_state) {
	$form_state['rebuild'] = TRUE;
	$values = $form_state['values'];
	if (isset($values['back']) && $values['op'] == $values['back']) {
		$form_state['storage'] = array();
	}
	else {
		$upload = file_load($form_state['values']['zip_file']);
		$cpmFile = 'zip://'.drupal_realpath($upload->uri).'#assets/js/CPM.js';
		$cpmContents = file_get_contents($cpmFile);
		$form_state['storage']['cpObj'] = captivate_import_parse_CPM($cpmContents);
		$form_state['storage']['step'] = 'captivate_import_process_upload_process';
	}
}

function captivate_import_process_upload_process($form, &$form_state) {
	$upload = file_load($form_state['values']['zip_file']);
	$slide_count = count($form_state['storage']['cpObj']->slides);
	$slide_images = array_map('captivate_import_process_get_images', $form_state['storage']['cpObj']->slides);
	$all_images = array_unique(call_user_func_array('array_merge', $slide_images));
	$repeated_images = array();
	foreach($all_images as $image) {
		$my_count = 0;
		foreach($slide_images as $slide) {
			in_array($image, $slide) ? $my_count++ : null;
		}
		$my_count > ((variable_get('captivate_import_repeat_threshhold')/100) * $slide_count) ? $repeated_images[$image] = $my_count : null;
	}
	$form['description_text'] = array(
		'#markup' => "<p>The following images show up on many slides. They may be master slide items or other pieces that shouldn't be transferred. How should they be handled in conversion?</p>",
	);
	$form_state['storage']['zip_file'] = $form_state['values']['zip_file'];
	$form_state['storage']['course'] = $form_state['values']['course'];
//	$form_state['storage']['repeated_images'] = $repeated_images;
	$image_cull_options = array(
		'keep' => t('Keep all'),
		'first' => t('Keep first only'),
		'skip' => t('Leave all out'),
	);
	$form['image_cull'] = array();
	$form['image_cull']['#tree'] = TRUE;
	foreach($repeated_images as $image => $count) {
		$imgFile = 'zip://'.drupal_realpath($upload->uri).'#'.$image;
		$imgData = base64_encode(file_get_contents($imgFile));
		$src = 'data: '.mime_content_type($imgFile).';base64, '.$imgData;
		$form['image_cull'][$image] = array(
			'#prefix' => "<img src=\"$src\" />",
			'#type' => 'radios',
			'#title' => t('Appears in ').$count.t(' of ').$slide_count.t(' slides'),
			'#options' => $image_cull_options,
			'#description' => t('Select what to do with this image element.'),
			'#default_value' => 'skip',
		);
	}
	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Next')
	);
	return $form;
}

function captivate_import_process_get_images($slide) {
	$imageList = array();
	foreach($slide->content->images as $key => $image) {
		$imageList[] = $image['ip'];
	}
	return ($imageList);
}

function captivate_import_process_upload_process_submit($form, &$form_state) {
	global $user;
	$image_cull = $form_state['values']['image_cull'];
	$course = taxonomy_term_load($form_state['storage']['course']);
	$upload = file_load($form_state['storage']['zip_file']);
	$filename = explode('.', $upload->filename);
	$destination = strtolower(preg_replace('/[^a-zA-Z0-9-]+/', '-', $course->name).'/'.preg_replace('/[^a-zA-Z0-9-]+/', '-', $filename[0]));
	$uniqueID = 0;
	while (file_exists($destination.'_'.$uniqueID)) {
		$uniqueID++;
	}
	$destination .= '_'.$uniqueID;
//	$form_state['redirect'] = 'node/'.$ewrapper->nid->value();
	//	Create batch
	$batch = array(
		'operations' => array(
			// Unzip the ZIP file.
			array('_captivate_import_process_unzip', array($upload->uri, $destination)),
			//  Create the book.
			array('_captivate_import_process_create_book', array($form_state['storage']['cpObj'])),
			//  Create the lunit node.
			array('_captivate_import_process_create_lunit_node', array($destination)),
			//  Prepare for slide conversion.
			array('_captivate_import_process_prepare_conversion', array()),
			//	Generate slide nodes based on that data.
			array('_captivate_import_process_generate_slide', array()),
			//  Generate keywords for each slide.
			array('_captivate_import_process_parse_keywords', array()),
			//  Extract and save images.
			array('_captivate_import_process_extract_images', array($image_cull, $destination)),
		),
		'init_message' => 'Extracting ZIP file to '.$destination,
		// Tell us how it all came out.
		'finished' => '_captivate_import_process_new_module_finished',
		'title' => t('Converting Captivate Module'),
		'error_message' => 'Error occurred during batch processing.',
	);
	//	Go go sing!.
	batch_set($batch);
}

/**
 * Implements hook_node_view_alter().
 * Load Captivate-oriented JavaScript.
 */
	
function captivate_import_process_node_view_alter(&$build) {
	if($build['#bundle'] == 'captivate_import_lunit' && $build['#view_mode'] == 'full') {
		// we are on full display for a lunit node.
		drupal_add_js(array('captivate_import' => array('cpPath' => file_create_url(variable_get('captivate_import_folder').$build['lunit_asset_path'][0]['#markup']))), 'setting');
		drupal_add_js(drupal_get_path('module', 'captivate_import').'/captivate_import.js');
		$build['lunit_asset_path'][0]['#type'] = 'markup';
		$build['lunit_asset_path'][0]['#prefix'] = '<div id="CPModuleWrapper" style="position: relative; width: 100%; padding: 0; margin: 0; padding-bottom: 56%; overflow: hidden; box-sizing: border-box"><div id="CPModuleInnerWrapper" style="position: absolute; left: 0; top: 0; right: 0; bottom: 0; border: 0; margin: 0; box-sizing: border-box"><div id="CPModuleContainer" style="position: relative; margin: 0; border: 0; width: 100%; height: 100%;  box-sizing: border-box">';
		$build['lunit_asset_path'][0]['#markup'] = '<iframe id="wrapperFrame" style="width: 100%; height: 100%; border: none; padding: 0" src="/'.drupal_get_path('module', 'captivate_import_process').'/captivate_import_viewer.php"></iframe>';
		$build['lunit_asset_path'][0]['#suffix'] = '</div></div></div>';
		$build['lunit_zip_file']['#title'] = 'Download module';
	}
} 

/**
 * Unpack ZIP file to location stored in node's lunit_asset_path field.
 *
 * @param $zipFile
 * Location of uploaded ZIP file
 *
 * @param $assetPath
 * Value of node's lunit_asset_path field.
 **/
 
function _captivate_import_process_unzip($zipFile, $assetPath, &$context) {
	$zip = new ZipArchive;
	$res = $zip->open(drupal_realpath($zipFile));
	if ($res === TRUE) {
		$zip->extractTo(variable_get('captivate_import_folder').$assetPath);
		$zip->close();
		$context['message'] = t($zipFile.' extracted successfully');
	}
	else {
		$result = array('status' => FALSE, 'data' => print_r($res, TRUE));
		die(json_encode($result));
	}
	$context['message'] = t('Analyzing CPM.js file');
}

/**
 * Create book via root page.
 **/
 
function _captivate_import_process_create_book($captivate, &$context) {
	global $language, $user;
	$page_values = array(
		'type' => 'book',
		'uid' => $user->uid,
		'status' => 1,
		'comment' => 1,
		'promote' => 0,
	);
	$entity = entity_create('node', $page_values);
	$ewrapper = entity_metadata_wrapper('node', $entity);
	$ewrapper->title->set($captivate->meta['projectVars']['pN']);
	$ewrapper->language($language->language);
	$entity->book = array(
		"bid" => 'new',
		"plid" => 0
	);
	$ewrapper->save();
	$context['results']['bid'] = $entity->book['bid'];
	$context['results']['mlid'] = $entity->book['mlid'];
	$context['results']['cpObj'] = $captivate;
}

/**
 * Create node for displaying Captivate module as published.
 **/

function _captivate_import_process_create_lunit_node($destination, &$context) {
	$captivate = $context['results']['cpObj'];
	global $language, $user ;
	$lunit_values = array(
		'type' => 'captivate_import_lunit',
		'uid' => $user->uid,
		'status' => 1,
		'comment' => 1,
		'promote' => 0,
	);
	$entity = entity_create('node', $lunit_values);
	$ewrapper = entity_metadata_wrapper('node', $entity);
	$ewrapper->title->set($captivate->meta['projectVars']['pN'] . ' (Captivate)');
	$ewrapper->lunit_developer->set($captivate->meta['cpVars']['cpInfoAuthor']);
	$ewrapper->lunit_email->set($captivate->meta['cpVars']['cpInfoEmail']);
	$ewrapper->lunit_asset_path->set($destination);
	$ewrapper->language($language->language);
	$entity->book = array(
		"bid" => $context['results']['bid'],
		"plid" => $context['results']['mlid'],
		"weight" => count($captivate->toc) + 1
	);
	$ewrapper->save();
	$context['message'] = t('Extracting slides.');
}

function _captivate_import_process_prepare_conversion(&$context) {
	$stopwords = explode(',', variable_get('captivate_import_stopwords'));
	array_walk($stopwords, function(&$wd) {
		$wd = ltrim($wd);
		$wd = rtrim($wd);
	});
	$context['results']['stopwordRegex'] = '/\b('.implode('|', $stopwords).'|\d+|.)\b/';
}

/**
 * Save slide nodes.
 *
 * @param $nodeID
 * node ID of the lunit to link the slides to.
 **/

function _captivate_import_process_generate_slide(&$context) {
	global $user, $language;
	if (!isset($context['sandbox']['progress'])) {
		$context['sandbox']['progress'] = 0;
		$context['sandbox']['current_node'] = 0;
		$context['sandbox']['max'] = count($context['results']['cpObj']->toc);
		$context['results']['slideNIDs'] = array();
		$context['results']['slideImages'] = array();
		$context['results']['slideTexts'] = array();
	}
// 	$slideData = array_shift($context['results']['cpObj']->slides);
	$tocData = $context['results']['cpObj']->toc[$context['sandbox']['progress']];
	$parentPage = $tocData['parent'] ? $context['results']['slideMLIDs'][$tocData['parent']] : $context['results']['mlid'];
	if ($tocData['link']) {
		$slideData = $context['results']['cpObj']->slides['Slide'.$tocData['link']];
	} else {
		$slideData = new stdClass();
		$slideData->lb = $tocData['text'];
		$slideData->dn = null;
		$slideData->content = new stdClass();
		$slideData->content->texts = array();
		$slideData->content->images = array();
	}
 	$context['message'] = t('Now creating Slide %slide of %total: %title', array('%slide' => $context['sandbox']['progress']+1, '%total' => $context['sandbox']['max'], '%title' => $tocData['text']));
	usort($slideData->content->texts);
	$slideBody = array('text' => '', 'accessibility' => '');
	foreach($slideData->content->texts as $aText) {
		if (!(preg_match('/^si/', $aText['mdi']))) {
			$aText['type'] == 589 ? $slideBody['headlines'][] = $aText['text'] : $slideBody['text'] .= $aText['text']."\r\n";
			$slideBody['accessibility'] .= $aText['accstr']."\r\n";
		}
	}
	$values = array(
		'type' => "captivate_import_slide",
		'uid' => $user->uid,
		'status' => 1,
		'comment' => 0,
		'promote' => 0
	);
	$slide_entity = entity_create('node', $values);
	$ewrapper = entity_metadata_wrapper('node', $slide_entity);
	$ewrapper->title->set($tocData['text']);
	$ewrapper->language($language->language);
	$ewrapper->slide_slide_number->set(intval($context['sandbox']['progress']+1));
	$ewrapper->slide_adobe_id->set($slideData->dn);
	$ewrapper->body->set(array('value' => $slideBody['text']));
	$ewrapper->slide_accessibility->set($slideBody['accessibility']);
	$ewrapper->slide_headline->set($slideBody['headlines']);
	$ewrapper->save();
	$slideNID = $ewrapper->nid->value();
	// create book page to point to slide because drupal's books can't handle multiple uses of pages
	$page_values = array(
		'type' => "book",
		'uid' => $user->uid,
		'status' => 1,
		'comment' => 0,
		'promote' => 0
	);
	$pageEntity = entity_create('node', $page_values);
	$page_wrapper = entity_metadata_wrapper('node', $pageEntity);
	$page_wrapper->title->set($tocData['text']);
	$pageEntity->book = array(
		"bid" => $context['results']['bid'],
		"plid" => $parentPage,
		"weight" => $context['sandbox']['progress']
	);
	$page_wrapper->slide_ref->set($slideNID);
	$page_wrapper->save();
	array_push($context['results']['slideImages'], array($ewrapper->nid->value() => $slideData->content->images));
	array_push($context['results']['slideTexts'], array($ewrapper->nid->value() => $slideData->content->texts));
	$context['results']['slideNIDs'][$context['sandbox']['progress']] = $slideNID;
	$context['results']['slideMLIDs'][$context['sandbox']['progress']] = $pageEntity->book['mlid'];
	$context['sandbox']['progress']++;
    $context['finished'] = $context['sandbox']['progress'] > $context['sandbox']['max'] ? 1 : $context['sandbox']['progress'] / $context['sandbox']['max'];
}

/**
 * Parse keywords from slide body.
 **/
 
function _captivate_import_process_parse_keywords(&$context) {
	global $language;
	if (!isset($context['sandbox']['progress'])) {
		$context['sandbox']['progress'] = 0;
		$context['sandbox']['current_node'] = 0;
		$context['sandbox']['max'] = count($context['results']['slideNIDs']);
	}
	$slide_node = node_load($context['results']['slideNIDs'][$context['sandbox']['progress']]);
	$slide_entity = entity_load_single('node', $context['results']['slideNIDs'][$context['sandbox']['progress']]);
	$slide_wrapper = entity_metadata_wrapper('node', $slide_entity);
	$slideText = $slide_wrapper->body->value->value();
	$slideText = preg_replace('/<[^>]+>/', '', $slideText);
 	$context['message'] = t('Generating keywords for Slide %slide of %total', array('%slide' => $context['sandbox']['progress']+1, '%total' => $context['sandbox']['max']));
	// here we actually get the keywords
	$keywords = strtolower(preg_replace($context['results']['stopwordRegex'], ' ', $slideText));
	$keywordVID = taxonomy_vocabulary_machine_name_load("keywords")->vid;
	preg_match_all("/(\w+\b)(?!.*\b\1\b)/", $keywords, $uniqueKeywords);
	// save them all into the taxonomy
	foreach($uniqueKeywords[0] as $keyword) {
		$term = taxonomy_get_term_by_name($keyword, 'keywords');
		if (!empty($term)) {
			$existing_term = array_values($term);
			$tid = $existing_term[0]->tid;
		} else {
			$new_term = new stdClass();
			$new_term->name = $keyword;
			$new_term->vid = $keywordVID;
			taxonomy_term_save($new_term);
			$tid = $new_term->tid;
		}
		$slide_wrapper->slide_keywords[] = $tid;
	}
	taxonomy_node_update($slide_node);
	$slide_wrapper->save();
	$context['sandbox']['progress']++;
    $context['finished'] = $context['sandbox']['progress'] > $context['sandbox']['max'] ? 1 : $context['sandbox']['progress'] / $context['sandbox']['max'];
}

/**
 * Extract images from slide objects.
 */
 
function _captivate_import_process_extract_images($image_cull, $assetPath, &$context) {
	global $language, $user;
	if (!isset($context['sandbox']['progress'])) {
		$context['sandbox']['progress'] = 0;
		$context['sandbox']['current_node'] = 0;
		$context['sandbox']['max'] = count($context['results']['slideImages']);
		$context['results']['createdFigures'] = array();
	}
 	$context['message'] = t('Extracting images for Slide %slide of %total', array('%slide' => $context['sandbox']['progress']+1, '%total' => $context['sandbox']['max']));
	$imageSlide = array_shift($context['results']['slideImages']);
	$textSlide = $context['results']['slideTexts'][$context['sandbox']['progress']];
	$potentialCaptions = array();
	foreach (array_shift($textSlide) as $textItem) {
		$potentialCaptions[] = array('text' => $textItem['text'], 'x' => $textItem['vb'][0], 'y' => $textItem['vb'][1]);
	}
	$slidekeys = array_keys($imageSlide);
	$slideNID = $slidekeys[0];
	$slide_node = node_load($slideNID);
	$savePath = variable_get('captivate_import_image_folder').$assetPath;
	file_prepare_directory($savePath, FILE_CREATE_DIRECTORY);
	$imageNum = 0;
	$cullImages = array_keys($image_cull);
	foreach ($imageSlide[$slideNID] as $imageArray) {
		$cull = FALSE;
		if (in_array($imageArray['ip'], $cullImages)) {
			switch($image_cull[$imageArray['ip']]) {
				case 'skip':
				$cull = TRUE;
				break;
				case 'keep':
				$cull = FALSE;
				break;
				case 'first':
				$cull = FALSE;
				$image_cull[$imageArray['ip']] = 'skip';
				break;
			}
		}
		if (!$cull) {
			$imageX = $imageArray['vb'][0];
			$imageY = $imageArray['vb'][3];
			$pulledCaption = null;
			$captionCount = 0;
			while ($pulledCaption == null && $captionCount < count($potentialCaptions)) {
				if (abs($potentialCaptions[$captionCount]['y'] - $imageY) < 10) {
					$pulledCaption = $potentialCaptions[$captionCount]['text'];
					$slide_entity = entity_load_single('node', $slideNID);
					$swrapper = entity_metadata_wrapper('node', $slide_entity);
					$swrapper->language($language->language);
					$oldBody = $swrapper->body->raw();
					$newBody = str_replace($pulledCaption, '', $oldBody['value']);
					dd('['.$oldBody['value'].']');
					dd('['.$pulledCaption.']');
					dd($newBody);
					dd('-----');
					$swrapper->body->set(array('value' => $newBody));
					$swrapper->save();
				} else {
					$captionCount++;
				}
			}
			if (in_array($imageArray['ip'], array_keys($context['results']['createdFigures']))) {
				$file = file_load($context['results']['createdFigures'][$imageArray['ip']]);
			} else {
				preg_match('/(?<=\/)[^\/]+$/', $imageArray['ip'], $filename);
				$image = file_get_contents(variable_get('captivate_import_folder').$assetPath.'/'.$imageArray['ip']);
				$file = file_save_data($image, variable_get('captivate_import_image_folder').$assetPath.'/'.$filename[0], FILE_EXISTS_REPLACE);
			}
			$values = array(
				'type' => "captivate_import_figure",
				'uid' => $user->uid,
				'status' => 1,
				'comment' => 0,
				'promote' => 0
			);
			$entity = entity_create('node', $values);
			$fwrapper = entity_metadata_wrapper('node', $entity);
			$fwrapper->language($language->language);
			$fwrapper->title->set('Slide '.$context['sandbox']['progress'].' Image '.$imageNum);
			$fwrapper->figure_caption->set($pulledCaption);
			$fwrapper->figure_file->file->set($file->fid);
			$fwrapper->figure_accessibility->set($imageArray['accstr']);
			$fwrapper->save();
			$context['results']['createdFigures'][$imageArray['ip']] = $file->fid;
			file_usage_add($file, 'captivate_import_process', 'node', $fwrapper->nid->value());
			$slide_node->slide_figures[$slide_node->language][] = array(
				'target_id' => $fwrapper->nid->value(),
				'target_type' => 'node'
			);
			node_save($slide_node);
			$imageNum++;
		}
	}
	$context['sandbox']['progress']++;
    $context['finished'] = $context['sandbox']['progress'] > $context['sandbox']['max'] ? 1 : $context['sandbox']['progress'] / $context['sandbox']['max'];
}

/**
 * Finish module creation and cleanup database.
 **/
 
function _captivate_import_process_new_module_finished() {
    db_query('TRUNCATE TABLE {node_comment_statistics}');
    $sql =<<<SQL
INSERT INTO
    node_comment_statistics
(
    nid,
    cid,
    last_comment_timestamp,
    last_comment_name,
    last_comment_uid,
    comment_count
)
SELECT
    n.nid,
    IFNULL(max_node_comment.max_cid,0) AS cid,
    IFNULL(last_comment.changed,n.changed) AS last_comment_timestamp,
    IFNULL(last_comment.name,null) AS last_comment_name,
    IFNULL(last_comment.uid,n.uid) AS last_comment_uid,
    IFNULL(comment_count.comment_count,0) AS comment_count
FROM
    node AS n
    LEFT OUTER JOIN (SELECT nid, COUNT(*) AS comment_count FROM comment WHERE status=1 GROUP BY nid) AS comment_count ON comment_count.nid=n.nid
    LEFT OUTER JOIN (SELECT nid, MAX(cid) AS max_cid FROM comment WHERE status=1 GROUP by nid) AS max_node_comment ON max_node_comment.nid=n.nid
    LEFT OUTER JOIN (SELECT cid,uid,name,changed FROM comment) AS last_comment ON last_comment.cid=max_node_comment.max_cid
WHERE
    n.status=1
ORDER BY
    n.nid
SQL;
    db_query($sql);
}


/**
 * Function to sort text objects.
 * Procedure: Sort by appearance time, then Y position, then X position.
 **/

function _captivate_import_process_text_sort($a, $b) {
	if ($a['from'] == $b ['from']) {
		// same time, evaluate Y next
		if ($a['vb'][1] == $b['vb'][1]) {
			// same Y, evaluate X
			if ($a['vb'][0] == $b['vb'][0]) {
				return 0;
			}
			return ($a['vb'][0] < $b['vb'][0]) ? -1 : 1;
		}
		return ($a['vb'][1] < $b['vb'][1]) ? -1 : 1;
	}
	return ($a['from'] < $b['from']) ? -1 : 1;
}
/**
 * Implements hook_node_info()
 */

function captivate_import_process_node_info() {
	return array(
		'captivate_import_lunit' => array(
			'name' => t('Learning Unit'),
			'base' => 'captivate_import_lunit',
			'description' => t('A Captivate learning unit'),
			'has_title' => TRUE,
			'title_label' => t('Unit Title')
		),
	);
}

/**
 * Implements hook_form
 **/
 
function captivate_import_lunit_form($node, &$form_state) {
//	dsm($form_state);
	return node_content_form($node, $form_state);
}